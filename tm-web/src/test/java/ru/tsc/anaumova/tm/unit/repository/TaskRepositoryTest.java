package ru.tsc.anaumova.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.anaumova.tm.config.ApplicationConfiguration;
import ru.tsc.anaumova.tm.marker.UnitCategory;
import ru.tsc.anaumova.tm.model.Task;
import ru.tsc.anaumova.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    private final Task task = new Task("test");

    @Before
    public void init() {
        task.setUserId(USER_ID);
        repository.save(task);
    }

    @After
    @Transactional
    public void destroy() {
        repository.delete(task);
    }

    @Test
    public void findByIdAndUserId() {
        @Nullable Task taskFromDB = repository.findByIdAndUserId(task.getId(), USER_ID).orElse(null);
        Assert.assertNotNull(taskFromDB);
        Assert.assertEquals(task.getName(), taskFromDB.getName());
    }

    @Test
    public void findAllByUserId() {
        List<Task> tasks = repository.findAllByUserId(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void countByUserId() {
        long count = repository.countByUserId(USER_ID);
        Assert.assertEquals(1, count);
    }

    @Test
    public void existsByIdAndUserId() {
        boolean exists = repository.existsByIdAndUserId(task.getId(), USER_ID);
        Assert.assertTrue(exists);
    }

    @Test
    @Transactional
    public void deleteByIdAndUserId() {
        repository.deleteByIdAndUserId(task.getId(), USER_ID);
        Assert.assertEquals(0, repository.countByUserId(USER_ID));
    }

    @Test
    @Transactional
    public void deleteAllByUserId() {
        repository.deleteAllByUserId(USER_ID);
        Assert.assertEquals(0, repository.countByUserId(USER_ID));
    }

}
