package ru.tsc.anaumova.tm.api.service.dto;

import ru.tsc.anaumova.tm.dto.model.ProjectDto;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {
}