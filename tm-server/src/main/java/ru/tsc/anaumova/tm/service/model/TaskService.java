package ru.tsc.anaumova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.anaumova.tm.api.service.model.ITaskService;
import ru.tsc.anaumova.tm.enumerated.Sort;
import ru.tsc.anaumova.tm.enumerated.Status;
import ru.tsc.anaumova.tm.exception.field.EmptyDescriptionException;
import ru.tsc.anaumova.tm.exception.field.EmptyIdException;
import ru.tsc.anaumova.tm.exception.field.EmptyNameException;
import ru.tsc.anaumova.tm.exception.field.EmptyUserIdException;
import ru.tsc.anaumova.tm.exception.system.EmptyStatusException;
import ru.tsc.anaumova.tm.model.Task;
import ru.tsc.anaumova.tm.repository.model.TaskRepository;
import ru.tsc.anaumova.tm.repository.model.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Override
    protected TaskRepository getRepository() {
        return repository;
    }

    @NotNull
    private UserRepository getUserRepository() {
        return userRepository;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final TaskRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(getUserRepository().findById(userId).orElse(null));
        @NotNull final TaskRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(getUserRepository().findById(userId).orElse(null));
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final TaskRepository repository = getRepository();
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.findAllByProjectIdAndUserId(userId, projectId);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRepository repository = getRepository();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final TaskRepository repository = getRepository();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final Task model) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final TaskRepository repository = getRepository();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskRepository repository = getRepository();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void update(@NotNull final Task model) {
        @NotNull final TaskRepository repository = getRepository();
        repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskRepository repository = getRepository();
        @Nullable final Task model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final TaskRepository repository = getRepository();
        @Nullable final Task model = repository.findFirstByUserIdAndId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.save(model);
        return model;
    }

    @Override
    public long getCount(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final TaskRepository repository = getRepository();
        return repository.countByUserId(userId);
    }

}