package ru.tsc.anaumova.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tsc.anaumova.tm.api.AuthEndpoint;
import ru.tsc.anaumova.tm.api.ProjectEndpoint;
import ru.tsc.anaumova.tm.api.TaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @NotNull
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public ServletRegistrationBean cxfServletRegistration() {
        return new ServletRegistrationBean(new CXFServlet(), "/ws/*");
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final ProjectEndpoint projectEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(@NotNull final TaskEndpoint taskEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(@NotNull final AuthEndpoint authEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Override
    protected void configure(@NotNull final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()
                .antMatchers("/api/auth/login").permitAll()
                .and()
                .authorizeRequests().anyRequest().authenticated()
                .and().formLogin()
                .and().logout().permitAll().logoutSuccessUrl("/login")
                .and().csrf().disable();
    }

}