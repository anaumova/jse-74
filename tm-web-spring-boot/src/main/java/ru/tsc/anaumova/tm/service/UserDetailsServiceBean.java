package ru.tsc.anaumova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.tsc.anaumova.tm.exception.field.EmptyLoginException;
import ru.tsc.anaumova.tm.model.CustomUser;
import ru.tsc.anaumova.tm.model.Role;
import ru.tsc.anaumova.tm.model.User;
import ru.tsc.anaumova.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        @Nullable final User user = findByLogin(username);
        @NotNull final org.springframework.security.core.userdetails.User.UserBuilder builder
                = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPasswordHash());
        @NotNull final List<Role> userRole = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        userRole.forEach(role -> roles.add(role.toString()));
        builder.roles(roles.toArray(new String[]{}));
        @NotNull final UserDetails details = builder.build();
        @NotNull final org.springframework.security.core.userdetails.User userSpring
                = (org.springframework.security.core.userdetails.User) details;
        @NotNull final CustomUser customUser = new CustomUser(userSpring, user);
        return customUser;
    }

    @Nullable
    private User findByLogin(@Nullable final String login) {
        return userRepository.findFirstByLogin(login).orElseThrow(EmptyLoginException::new);
    }

}