package ru.tsc.anaumova.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.anaumova.tm.marker.WebCategory;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.model.Result;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Category(WebCategory.class)
public class ProjectRestEndpointClientTest {

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project/";

    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();

    @NotNull
    private final Project project = new Project();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=admin&password=admin";
        HEADER.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response =
                restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        @Nullable final String sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
    }

    private static <T> ResponseEntity<T> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity,
            @NotNull final Class<T> responseType
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @Before
    public void init() {
        @NotNull final String url = PROJECT_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project, HEADER), Project.class);
    }

    @After
    public void destroy() {
        @NotNull final String url = PROJECT_URL + "clear/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER), Project.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(HEADER), Result.class);
    }

    private long count() {
        @NotNull final String logoutUrl = PROJECT_URL + "count/";
        @NotNull final ResponseEntity<Long> response =
                sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(HEADER), Long.class);
        if (response.getStatusCode() != HttpStatus.OK) return 0;
        @Nullable Long result = response.getBody();
        if (result == null) return 0;
        return result;
    }

    @Test
    public void findAll() {
        @NotNull final String url = PROJECT_URL + "findAll/";
        @NotNull final ResponseEntity<List> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), List.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final List projectList = response.getBody();
        Assert.assertNotNull(projectList);
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void findById() {
        @NotNull final String url = PROJECT_URL + "findById/" + project.getId();
        @NotNull final ResponseEntity<Project> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final Project projectFind = response.getBody();
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    public void existsById() {
        @NotNull String url = PROJECT_URL + "existsById/" + project.getId();
        @NotNull ResponseEntity<Boolean> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable Boolean result = response.getBody();
        Assert.assertNotNull(result);
        Assert.assertTrue(result);

        url = PROJECT_URL + "existsById/" + UUID.randomUUID();
        response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        result = response.getBody();
        Assert.assertNotNull(result);
        Assert.assertFalse(result);
    }

    @Test
    public void save() {
        @NotNull final String url = PROJECT_URL + "save/";
        @NotNull final Project projectSave = new Project();
        @NotNull final ResponseEntity<Project> response =
                sendRequest(url, HttpMethod.POST, new HttpEntity<>(projectSave, HEADER), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @NotNull final String findUrl = PROJECT_URL + "findById/" + projectSave.getId();
        @NotNull final ResponseEntity<Project> responseFind = sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER), Project.class);
        Assert.assertNotNull(responseFind.getBody());
        @NotNull final Project projectFind = responseFind.getBody();
        Assert.assertEquals(projectSave.getId(), projectFind.getId());
    }

    @Test
    public void delete() {
        Assert.assertEquals(1, count());
        @NotNull final String url = PROJECT_URL + "delete/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project, HEADER), Project.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteAll() {
        Assert.assertEquals(1, count());
        List<Project> projects = new ArrayList<>();
        projects.add(project);
        @NotNull final String url = PROJECT_URL + "deleteAll/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(projects, HEADER), Project.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void clear() {
        Assert.assertEquals(1, count());
        @NotNull final String url = PROJECT_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER), Project.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteById() {
        Assert.assertEquals(1, count());
        @NotNull final String url = PROJECT_URL + "deleteById/" + project.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADER), Project.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

}